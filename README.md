# Game Development
The following is a step by step build up of a game using pure javascript.
Topics Include:

## Collision Detection
Detecting when entities cross paths

## KeyDown Events
Detecting keyboard and mouse events

## Automation
Having objects moving programmatically

## Firing Events
Creating objects dynamically