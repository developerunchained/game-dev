//Set Flag
var entitySelected = "entityA";
var DIRECTION = "RIGHT";
var COUNTER = 0;

//Set Key Listener
document.addEventListener("keydown", function(event) {
  
  var moveEntity = 'entityA';
  var entity = document.getElementById('entityA');
  var entity_styles  = getComputedStyle(entity);

  checkPositionStatus();
  
  switch(event.keyCode) {
	   
	 case 37: 
	      // left arrow
	      var newVal = parseInt(entity_styles.getPropertyValue("left").replace('px','')) - 5;
		  entity.style.left = newVal +"px";
	      break;
     case 38: 
	      // up arrow
	      var newVal =  parseInt(entity_styles.getPropertyValue("top").replace('px',''))  - 5;
		  entity.style.top = newVal +"px";
	      break;	  
     case 39: 
	      // right arrow
	      var newVal = parseInt(entity_styles.getPropertyValue("left").replace('px',''))  + 5;
		  entity.style.left = newVal +"px";
	      break;
     case 40: 
	      // down arrow
	      var newVal =  parseInt(entity_styles.getPropertyValue("top").replace('px',''))  + 5;
		  entity.style.top = newVal + "px";
	      break;
     case 32: 
          //space bar
          console.log("fire");		  
		  //Let's create a bullet and fire
		  var newVal =  parseInt(entity_styles.getPropertyValue("top").replace('px',''));
		  top_pos = newVal + "px";
		  newVal   = parseInt(entity_styles.getPropertyValue("left").replace('px',''));
		  left_pos = newVal + "px";
		 
		  
		  var bullet = document.createElement("div");
		  bullet.setAttribute("id","thisBullet");
		  bullet.style.width="30px";
		  bullet.style.height="30px";
		  bullet.style.background="#DAA520";
		  bullet.style.position="absolute";
		  bullet.style.top= top_pos;
		  bullet.style.left= left_pos;
		  document.body.appendChild(bullet);

		  
		  var timesRun = 0;
			var interval = setInterval(function(){
				timesRun += 1;
				if(timesRun === 200){
					clearInterval(interval);
					document.body.removeChild(bullet);
				}
				fireBullet(moveEntity,bullet);
			}, 0); 
		  break;
  }
});


function fireBullet(moveEntity) {
	
	var bull = document.getElementById('thisBullet');
	var bullet_styles  = getComputedStyle(bull);
		  if(moveEntity == "entityA") {
		       var newVal = parseInt(bullet_styles.getPropertyValue("left").replace('px','')) + 5;
		     bull.style.left = newVal +"px";
		  } else {
			  	var newVal = parseInt(bullet_styles.getPropertyValue("left").replace('px','')) - 5;
		         bull.style.left = newVal +"px";
		  }
}

//check distance between entities
function checkPositionStatus() {
	
	var entityA = document.getElementById('entityA');
	var entityB = document.getElementById('entityB');
	
	var entityAPosition = {
		left: parseInt(getComputedStyle(entityA).getPropertyValue("left").replace('px','')),
		top: parseInt(getComputedStyle(entityA).getPropertyValue("top").replace('px',''))		
	}
	var entityBPosition = {
		left: parseInt(getComputedStyle(entityB).getPropertyValue("left").replace('px','')),
		top: parseInt(getComputedStyle(entityB).getPropertyValue("top").replace('px',''))		
	}
	
	distanceLeft = Math.abs(entityAPosition.left - entityBPosition.left);
	distanceTop = Math.abs(entityAPosition.top - entityBPosition.top);
	
	if((distanceTop <= 205) && (distanceLeft <=205)) {
		document.getElementById('alertMechanism').innerHTML  = "COLLISION DETECTED";
	} else {
		document.getElementById('alertMechanism').innerHTML  = "";
	}
}

function automateEnemy() {
		
	var entity = document.getElementById('entityB');
	var entity_styles  = getComputedStyle(entity);
	var timeNow = new Date();
		
    if(COUNTER == 30) {
		determineEnemyDirection();
		COUNTER = 0;
	}
		
	switch(DIRECTION) {
	 case "LEFT": 
	      // left arrow
	      var newVal = parseInt(entity_styles.getPropertyValue("left").replace('px','')) - 5;
		  entity.style.left = newVal +"px";
	      break;
     case "UP": 
	      // up arrow
	      var newVal =  parseInt(entity_styles.getPropertyValue("top").replace('px',''))  - 5;
		  entity.style.top = newVal +"px";
	      break;	  
     case "RIGHT": 
	      // right arrow
	      var newVal = parseInt(entity_styles.getPropertyValue("left").replace('px',''))  + 5;
		  entity.style.left = newVal +"px";
	      break;
     case "DOWN": 
	      // down arrow
	      var newVal =  parseInt(entity_styles.getPropertyValue("top").replace('px',''))  + 5;
		  entity.style.top = newVal + "px";
	      break;
    }
	COUNTER++;
}

function determineEnemyDirection() {
	
	var randomDirections = ['UP','DOWN','LEFT','RIGHT'];
	var newDirection = randomDirections[Math.floor(Math.random() * randomDirections.length)];
	
	if(newDirection == DIRECTION) {
		switch(newDirection) {
			case "LEFT":
			    DIRECTION  = "RIGHT";
				break;
			case "RIGHT":
			    DIRECTION  = "LEFT";
				break;
			case "UP":
			    DIRECTION  = "DOWN";
				break;
			case "DOWN":
			    DIRECTION  = "UP";
				break;
		}
	} else {
		DIRECTION = newDirection;
	}
}

setInterval(automateEnemy, 100);